import scraping.Scraper;

import java.io.IOException;

public class Main{
    public static void main (String[] args) throws IOException {
        Scraper scraper = new Scraper();
        scraper.initializeParties();
        scraper.getDocumentByDate(3,12,2019);
    }
}
