package scraping;

import models.Category;
import models.Party;
import models.Politician;
import models.Votable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Scraper {
    private List<Category> categories = new ArrayList<Category>();
    private final String BASE_URL = "https://www.tweedekamer.nl";
    private final String PARTIES_URL = BASE_URL + "/kamerleden_en_commissies/fracties";
    private final String VOTABLES_URL = BASE_URL + "/kamerstukken/stemmingsuitslagen?qry=*&Type=Kamerstukken&fld_tk_categorie=Kamerstukken&fld_tk_subcategorie=Stemmingsuitslagen&srt=date:desc:date,prl_volgorde:asc:num&fromdate=%d-%d-%d&todate=%d-%d-%d";
    private List<Party> parties = new ArrayList<>();

    public void getDocumentByDate(int day, int month, int year) throws IOException {
        String newUrl = String.format(VOTABLES_URL, day , month, year, day, month, year);
        Document document;
        try {
            document = Jsoup.connect(newUrl).get();
        }
        catch (Exception e) {
            System.out.println(e.toString());
            return;
        }
        getCategoriesByDocument(document);
    }
    public List<Category> getCategoriesByDocument(Document document) {
        List<Element> elements = document.getElementsByClass("card-content-list-item");
        for (Element element : elements) {
            String title = element.getElementsByClass("card-content-list-item__title").html();
            String url = BASE_URL + element.attributes().get("href");
            Document categoryDocument;
            try {
                categoryDocument = Jsoup.connect(url).get();
            }
            catch (Exception e){
                System.out.println(e.toString());
                break;
            }


            Category category = new Category(title);
            category.setVotables(getVotablesByDocument(categoryDocument));
            categories.add(category);
            System.out.println(element.getElementsByClass("card-content-list-item__title").html());
            System.out.println(element.attributes().get("href"));
        }
        return null;
    }
    private List<Votable> getVotablesByDocument(Document document) {
        List<Element> cards = document.getElementsByClass("card-container");
        ArrayList<Votable> votables = new ArrayList<>();
        for (Element card : cards) {
            String type = card.getElementsByClass("card__title").html();
            String date = card.getElementsByClass("card__pretitle").html();
            String url = BASE_URL + card.attributes().get("href");
            System.out.println("-- " + type + " " + date);
        }
        return votables;
    }
    private Votable getVotableByLink (String url) {
        Document votableDocument;
        try {
            votableDocument = Jsoup.connect(url).get();
        }
        catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        String type = votableDocument.getElementsByClass("section__caption").html();
        String title = votableDocument.getElementsByClass("section__title").html();
        ArrayList<Politician> submitters = getSubmittersByDocument(votableDocument);
        ArrayList<Politician> votersFor = getVotersForByDocument(votableDocument);
        ArrayList<Politician> votersAgainst = getVotersAgainstByDocument(votableDocument);
        Element greyList = votableDocument.getElementsByClass("___grey").get(0);
        String date = greyList.getElementsByClass("link-list__text").get(0).html();
        return new Votable(title, submitters, type, votersFor, votersAgainst, date);
    }


    private ArrayList<Politician> getVotersForByDocument (Document document) {
        Element leftSide = document.getElementsByClass("col-sm-6").get(0);
        for (Element row : leftSide.getElementsByTag("tr")) {
            String partyName = row.getElementsByClass("td").get(0).getElementsByTag("span").get(0).html();
            String amount = row.getElementsByClass("td").get(1).getElementsByTag("span").get(0).html();
            Party party = getPartyByAltName(partyName);
            return (ArrayList<Politician>) party.getMembers();
        }
        return null;
    }

    private ArrayList<Politician> getVotersAgainstByDocument (Document document) {
        Element rightSide = document.getElementsByClass("col-sm-3").get(0);
        for (Element row : rightSide.getElementsByTag("tr")) {
            String partyName = row.getElementsByClass("td").get(0).getElementsByTag("span").get(0).html();
            String amount = row.getElementsByClass("td").get(1).getElementsByTag("span").get(0).html();
            Party party = getPartyByAltName(partyName);
            return (ArrayList<Politician>)party.getMembers();
        }
        return null;
    }

    private Party getPartyByAltName(String altName) {
        for (Party party : parties) {
            if (party.getAltName() == altName) {
                return party;
            }
        }
        //TODO:add error message
        return null;
    }


    private Politician getPoliticianByName(String name) {
        for (Party party : parties) {
            for (Politician member : party.getMembers()) {
                if (member.getName().equals(name)) return member;
            }
        }
        //TODO:add error message
        return null;
    }

    private ArrayList<Politician> getSubmittersByDocument (Document document) {
        ArrayList<Politician> submitters = new ArrayList<>();
        Element content = document.getElementsByClass("content").get(0);
        for (Element element : content.getElementsByClass("link-list__text")) {
            String name = element.getElementsByTag("a").get(0).html();
            String party = element.getElementsByTag("a").get(1).html();
            Politician politician = getPoliticianByName(name);
            submitters.add(politician);
        }
        return submitters;
    }
    public void initializeParties () {
        Document partiesDocument;
        try {
            partiesDocument = Jsoup.connect(PARTIES_URL).get();
        }
        catch (Exception e){
            System.out.println(e.toString());
            return;
        }
        for(Element element : partiesDocument.getElementsByClass("gov-party__party-name")) {
            String url = BASE_URL + element.attributes().get("href");
            Document partyDocument;
            try {
                partyDocument = Jsoup.connect(url).get();
            }
            catch (Exception e) {
                System.out.println(e.toString());
                break;
            }
            this.parties.add(getPartyByDocument(partyDocument));
        }
    }


    private Party getPartyByDocument (Document document) {
        List<Politician> politicians = getPartyMembersMyDocument(document);

        Element partyInfoCard = document.getElementsByClass("___push-up-md").get(0);
        String nameAndParty = partyInfoCard.getElementsByTag("b").get(0).html();
        String name = nameAndParty.split("\\(")[0];
        String altPartyName = nameAndParty.split("\\(")[1].split("\\)")[0];

        Politician partyLeader = new Politician(name);
        String partyName =getPartyNameByDocument(document);
        for (Politician politician : politicians) {
            if (politician.getName().equals(partyLeader.getName())) politicians.remove(politician);
        }
        politicians.add(partyLeader);
        return new Party(politicians, partyLeader, partyName, altPartyName);
    }

    private String getPartyNameByDocument (Document document) {
        String partyName = document.getElementsByClass("section__title").get(0).html();
        return partyName;
    }

    private Politician getPartyLeaderByDocument(Document document) {
        Element partyInfoCard = document.getElementsByClass("___push-up-md").get(0);
        String nameAndParty = partyInfoCard.getElementsByTag("b").get(0).html();
        String name = nameAndParty.split("\\(")[0];
        String altName = nameAndParty.split("\\(")[1].split("\\)")[0];
        return new Politician(name);
    }

    private List<Politician> getPartyMembersMyDocument (Document document) {
        ArrayList<Politician> politicians = new ArrayList<>();
        for (Element member : document.getElementsByClass("toggle-member-info")) {
            Politician politician = new Politician(member.html());
            politicians.add(politician);
        }
        return politicians;
    }
}
