package interfaces;

import models.Category;

public interface CategoryDataAccess {
    void addCategory(Category category);
    void removeCategory(Category category);
    Category getCategoryByName(String name);
}
