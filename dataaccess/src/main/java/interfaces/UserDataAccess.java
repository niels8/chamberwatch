package interfaces;

import models.User;

public interface UserDataAccess {
    void logIn();
    User getUserByUsername(String username);
    void addUser(User user);
}
