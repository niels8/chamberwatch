package interfaces;

import models.Politician;

public interface PoliticianDataAccess {
    void addPolitician(Politician politician);
    void removePolitician(Politician politician);
}
