package logic.businesslogic;

import entities.CommentEntity;
import entities.VoteEntity;
import models.CommentVote;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.find;
@Transactional
@ApplicationScoped
public class CommentVoteLogic {
    public void addCommentVote(CommentVote vote, int commentId, String userId) {
        CommentEntity comment = (CommentEntity) find("commentId", commentId);
        VoteEntity voteToAdd = new VoteEntity();
        voteToAdd.comment = comment;
        voteToAdd.userId = userId;
        voteToAdd.voteType = vote.voteType;
        voteToAdd.persist();
    }
    public void removeCommentVote(CommentVote vote, String userId) {
        if (vote.userId == userId) {
            VoteEntity voteToRemove = (VoteEntity)VoteEntity.find("voteId", vote.voteId);
            voteToRemove.delete();
        }
        else {
            //TODO:error
        }
    }
}
