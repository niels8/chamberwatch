package logic.businesslogic;

import entities.CategoryEntity;
import entities.PoliticianEntity;
import entities.VotableEntity;
import entities.VotableVoteEntity;
import models.Politician;
import models.Votable;
import models.VotableVote;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Transactional
@ApplicationScoped
public class VotableLogic {
    CategoryLogic categoryLogic = new CategoryLogic();
    VotableVoteLogic votableVoteLogic = new VotableVoteLogic();
    public Votable getVotableById(int categoryId, int votableId) {
        for (Votable votable : categoryLogic.getCategoryById(categoryId).votables) {
            if (votable.votableId == votableId) {
                return votable;
            }
        }
        return null;
        //TODO: error
    }
    public void addVotable (Votable votable, int categoryId) {
        VotableEntity votableToAdd = new VotableEntity();
        votableToAdd.name = votable.name;
        votableToAdd.category = CategoryEntity.findById(categoryId);
        votableToAdd.date = votable.date;
        votableToAdd.status = votable.status;
        Set<PoliticianEntity> newSubmitters = new HashSet<>();
        for (Politician submitter : votable.submitters) {
            newSubmitters.add(PoliticianEntity.findById(submitter.politicianId));
        }
        votableToAdd.submitters = newSubmitters;
        votableToAdd.type = votable.type;
        votableToAdd.persist();
        if (votable.votes.size() > 0) {
            for (VotableVote vote : votable.votes) {
                votableVoteLogic.addVotableVote(vote, votableToAdd.votableId);
            }
        }
    }
}
