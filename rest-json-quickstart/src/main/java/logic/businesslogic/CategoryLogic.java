package logic.businesslogic;

import entities.CategoryEntity;
import entities.VotableEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;
import models.Category;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
@Transactional
@ApplicationScoped
public class CategoryLogic {
    private ModelMapper mapper = new ModelMapper();
    public Category addCategory(Category category) {
        CategoryEntity categoryToAdd = new CategoryEntity();
        categoryToAdd.name = category.name;
        categoryToAdd.persistAndFlush();
        return mapper.map(categoryToAdd, Category.class);
    }
    public void removeCategory(Category category) {
        CategoryEntity categoryToRemove = (CategoryEntity) CategoryEntity.find("categoryId", category.categoryId);
        if(categoryToRemove == null) {
            //TODO: error
        }
        if (categoryToRemove.votables.size() != 0) {
            categoryToRemove.votables.forEach(PanacheEntityBase::delete);
        }
        categoryToRemove.delete();
    }


    public Category getCategoryById(int categoryId) {
        Category category = mapper.map(CategoryEntity.find("categoryId", categoryId), Category.class);
        return category;
    }
    public List<Category> getCategories () {
        List<CategoryEntity> categories = CategoryEntity.list("*", Sort.ascending("date"));
        List<Category> returnList = mapper.map(categories, new TypeToken<List<Category>>(){}.getType());
        return returnList;
    }
    public List<Category> getCategoriesByDate(String date) {
        List<VotableEntity> votables = VotableEntity.list("date", date);
        List<CategoryEntity> categories = new ArrayList<>();
        for (VotableEntity votable : votables) {
            categories.add(votable.category);
        }
        List<CategoryEntity> uniqueCategories = new ArrayList<>(new HashSet<>(categories));
        return mapper.map(uniqueCategories, new TypeToken<List<Category>>(){}.getType());
    }
}
