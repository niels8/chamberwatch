package logic.businesslogic;

import entities.CommentEntity;
import entities.VotableEntity;
import models.Comment;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@Transactional
@ApplicationScoped
public class CommentLogic {
    ModelMapper mapper = new ModelMapper();
    public void addComment(int votableId, Comment comment, String userId) {
        VotableEntity votable = (VotableEntity)VotableEntity.find("votableId", votableId);
        CommentEntity commentToAdd = new CommentEntity();
        commentToAdd.body = comment.body;
        commentToAdd.score = 0;
        commentToAdd.votable = votable;
        commentToAdd.userId = userId;
        commentToAdd.persist();
    }
    public void removeComment(int commentId, String userId) {
        CommentEntity commentToRemove = (CommentEntity)CommentEntity.find("commentId", commentId);
        if (commentToRemove.userId != userId) {
            //TODO:error
        }
        else {
            commentToRemove.delete();
        }
    }
}
