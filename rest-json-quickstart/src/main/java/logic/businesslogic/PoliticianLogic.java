package logic.businesslogic;

import entities.PoliticianEntity;
import models.Politician;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class PoliticianLogic {
    ModelMapper mapper = new ModelMapper();
    public Politician getPoliticianByName(String name){
        PoliticianEntity politician = PoliticianEntity.find("name", name).firstResult();
        if (politician == null) {
            //TODO:error
            return null;
        }
        else {
            return mapper.map(politician, Politician.class);
        }
    }
    public Politician addPolitician(Politician politician) {
        PoliticianEntity politicianToAdd = new PoliticianEntity();
        politicianToAdd.name = politician.name;
        politicianToAdd.persist();
        return mapper.map(politicianToAdd, Politician.class);
    }
}
