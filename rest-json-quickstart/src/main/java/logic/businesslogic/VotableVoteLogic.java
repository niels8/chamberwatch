package logic.businesslogic;

import entities.PoliticianEntity;
import entities.VotableEntity;
import entities.VotableVoteEntity;
import models.VotableVote;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@Transactional
@ApplicationScoped
public class VotableVoteLogic {
    public void addVotableVote (VotableVote vote, int votableId) {
        VotableEntity votable = (VotableEntity) VotableEntity.find("votableId", votableId);
        PoliticianEntity politician = (PoliticianEntity) PoliticianEntity.find("politicianId", vote.politician.politicianId);
        VotableVoteEntity voteToAdd = new VotableVoteEntity();
        voteToAdd.isFor = vote.isFor;
        voteToAdd.politician = politician;
        voteToAdd.votable = votable;
        voteToAdd.persist();
    }

    public void removeVotableVote (VotableVote vote) {
        VotableVoteEntity voteToRemove = (VotableVoteEntity) VotableVoteEntity.find("id", vote.id);
        if (voteToRemove == null) {

        }
        else {
            voteToRemove.delete();
        }
    }
}
