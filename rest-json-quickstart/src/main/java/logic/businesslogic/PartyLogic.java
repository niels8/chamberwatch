package logic.businesslogic;

import entities.PartyEntity;
import entities.PoliticianEntity;
import models.Party;
import models.Politician;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Transactional
@ApplicationScoped
public class PartyLogic {
    private ModelMapper mapper = new ModelMapper();
    private PoliticianLogic politicianLogic = new PoliticianLogic();
    public void addParty(Party party) {
        PartyEntity partyToAdd = new PartyEntity();
        Set<PoliticianEntity> politicians = new HashSet<>();
        for (Politician member : party.members) {
            PoliticianEntity politician = PoliticianEntity.findById(politicianLogic.addPolitician(member).politicianId);
            politicians.add(politician);
        }
        partyToAdd.members = politicians;
        partyToAdd.name = party.name;
        partyToAdd.shortname = party.altName;
        partyToAdd.persist();
        partyToAdd.partyLeader = PoliticianEntity.find("name", party.partyLeader.name).firstResult();

    }
    public void removeParty(int partyId) {
        PartyEntity partyToRemove = (PartyEntity) PartyEntity.find("partyId", partyId);
        if (partyToRemove == null) {

        }
        else {
            partyToRemove.delete();
        }
    }
    public Party getPartyById(int partyId) {
        PartyEntity party = (PartyEntity) PartyEntity.find("partyId", partyId);
        if (party == null) {
            //TODO:error
            return null;
        }
        else {
            return mapper.map(party, Party.class);
        }
    }
    public Party getPartyByShortname(String shortname) {
        PartyEntity party = (PartyEntity) PartyEntity.find("shortname", shortname);
        if (party == null) {
            //TODO: error
            return null;
        }
        else {
            return mapper.map(party, Party.class);
        }
    }
}
