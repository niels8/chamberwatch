package logic.webscraping;

import logic.businesslogic.CategoryLogic;
import logic.businesslogic.PartyLogic;
import logic.businesslogic.PoliticianLogic;
import logic.businesslogic.VotableLogic;
import models.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@ApplicationScoped
public class CategoriesScraper {
    private List<Category> categories = new ArrayList<Category>();
    private final String BASE_URL = "https://www.tweedekamer.nl";
    private final String VOTABLES_URL = BASE_URL + "/kamerstukken/stemmingsuitslagen?qry=*&Type=Kamerstukken&fld_tk_categorie=Kamerstukken&fld_tk_subcategorie=Stemmingsuitslagen&srt=date:desc:date,prl_volgorde:asc:num&fromdate=%d-%d-%d&todate=%d-%d-%d";
    private CategoryLogic categoryLogic = new CategoryLogic();
    private PartyLogic partyLogic = new PartyLogic();
    private PoliticianLogic politicianLogic = new PoliticianLogic();
    private VotableLogic votableLogic = new VotableLogic();

    public void scrapeForDate(int day, int month, int year) throws IOException {
        saveCategories(getCategoriesByDate(day, month, year));
    }

    public void saveCategories (List<Category> categories) {
        for (Category category : categories) {
            Category addedCategory = categoryLogic.addCategory(category);
            for (Votable votable : category.votables) {
                votableLogic.addVotable(votable, addedCategory.categoryId);
            }
        }
    }

    public List<Category> getCategoriesByDate(int day, int month, int year) throws IOException {
        String newUrl = String.format(VOTABLES_URL, day , month, year, day, month, year);
        Document document;
        try {
            document = Jsoup.connect(newUrl).get();
        }
        catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        return getCategoriesByDocument(document);
    }
    public List<Category> getCategoriesByDocument(Document document) {
        List<Element> elements = document.getElementsByClass("card-content-list-item");
        for (Element element : elements) {
            String title = element.getElementsByClass("card-content-list-item__title").html();
            String url = BASE_URL + element.attributes().get("href");
            Document categoryDocument;
            try {
                categoryDocument = Jsoup.connect(url).get();
            }
            catch (Exception e){
                System.out.println(e.toString());
                break;
            }
            System.out.println(url);
            Category category = new Category(title);
            category.setVotables(getVotablesByDocument(categoryDocument));
            categories.add(category);
        }
        return categories;
    }
    private List<Votable> getVotablesByDocument(Document document) {
        List<Element> cards = document.getElementsByClass("card");
        System.out.println(cards.size());
        ArrayList<Votable> votables = new ArrayList<>();
        for (Element card : cards) {
            System.out.println(card.html());
            String url = BASE_URL + card.attributes().get("href");
            votables.add(getVotableByLink(url));
        }
        return votables;
    }
    private Votable getVotableByLink (String url) {
        Document votableDocument;
        try {
            votableDocument = Jsoup.connect(url).get();
        }
        catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        String type = votableDocument.getElementsByClass("section__caption").html();
        String title = votableDocument.getElementsByClass("section__title").html();
        System.out.println(url);
        ArrayList<Politician> submitters = getSubmittersByDocument(votableDocument);

        List<VotableVote> votes = new ArrayList<>();
        ArrayList<Politician> votersFor = getVotersForByDocument(votableDocument);
        ArrayList<Politician> votersAgainst = getVotersAgainstByDocument(votableDocument);
        for (Politician politician : votersFor) {
            VotableVote vote = new VotableVote();
            vote.isFor = true;
            vote.politician = politician;
            votes.add(vote);
        }
        for (Politician politician : votersAgainst) {
            VotableVote vote = new VotableVote();
            vote.isFor = false;
            vote.politician = politician;
            votes.add(vote);
        }
        Element greyList = votableDocument.getElementsByClass("___grey").get(0);
        String date = greyList.getElementsByClass("link-list__text").get(0).html();
        return new Votable(title, type, date, submitters, votes);
    }


    private ArrayList<Politician> getVotersForByDocument (Document document) {
        Element leftSide = document.getElementsByClass("col-sm-6").get(0);
        for (Element row : leftSide.getElementsByTag("tr")) {
            String partyName = row.getElementsByClass("td").get(0).getElementsByTag("span").get(0).html();
            Party party = partyLogic.getPartyByShortname(partyName);

            return (ArrayList<Politician>) party.members;
        }
        return null;
    }

    private ArrayList<Politician> getVotersAgainstByDocument (Document document) {
        Element rightSide = document.getElementsByClass("col-sm-3").get(0);
        for (Element row : rightSide.getElementsByTag("tr")) {
            String partyName = row.getElementsByClass("td").get(0).getElementsByTag("span").get(0).html();
            Party party = partyLogic.getPartyByShortname(partyName);
            return (ArrayList<Politician>)party.members;
        }
        return null;
    }

    private ArrayList<Politician> getSubmittersByDocument (Document document) {
        ArrayList<Politician> submitters = new ArrayList<>();
        Element content = document.getElementsByClass("content").get(0);
        for (Element element : content.getElementsByClass("link-list__text")) {
            String name = element.getElementsByTag("a").get(0).html();
            Politician politician = politicianLogic.getPoliticianByName(name);
            submitters.add(politician);
        }
        return submitters;
    }

}
