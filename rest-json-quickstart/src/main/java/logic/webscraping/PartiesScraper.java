package logic.webscraping;

import logic.businesslogic.PartyLogic;
import models.Party;
import models.Politician;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
@ApplicationScoped
@Transactional
public class PartiesScraper {
    private final String BASE_URL = "https://www.tweedekamer.nl";
    private final String PARTIES_URL = BASE_URL + "/kamerleden_en_commissies/fracties";
    PartyLogic partyLogic = new PartyLogic();
    public void saveParties(List<Party> parties) {
        for (Party party : parties) {
            partyLogic.addParty(party);
        }
    }

    public void initializeParties () {
        List<Party> parties = new ArrayList<>();
        Document partiesDocument;
        try {
            partiesDocument = Jsoup.connect(PARTIES_URL).get();
        }
        catch (Exception e){
            System.out.println(e.toString());
            return;
        }
        for(Element element : partiesDocument.getElementsByClass("gov-party__party-name")) {
            String url = BASE_URL + element.attributes().get("href");
            Document partyDocument;
            try {
                partyDocument = Jsoup.connect(url).get();
            }
            catch (Exception e) {
                System.out.println(e.toString());
                break;
            }
            parties.add(getPartyByDocument(partyDocument));
        }
        saveParties(parties);
    }


    private Party getPartyByDocument (Document document) {
        List<Politician> politicians = getPartyMembersMyDocument(document);

        Element partyInfoCard = document.getElementsByClass("___push-up-md").get(0);
        String nameAndParty = partyInfoCard.getElementsByTag("b").get(0).html();
        String name = nameAndParty.split("\\(")[0];
        String altPartyName = nameAndParty.split("\\(")[1].split("\\)")[0];

        Politician partyLeader = new Politician();
        partyLeader.name = name;
        String partyName =getPartyNameByDocument(document);
        for (Politician politician : politicians) {
            if (politician.name.equals(partyLeader.name)) politicians.remove(politician);
        }
        politicians.add(partyLeader);
        return new Party(politicians, partyLeader, partyName, altPartyName);
    }

    private String getPartyNameByDocument (Document document) {
        String partyName = document.getElementsByClass("section__title").get(0).html();
        return partyName;
    }

    private Politician getPartyLeaderByDocument(Document document) {
        Element partyInfoCard = document.getElementsByClass("___push-up-md").get(0);
        String nameAndParty = partyInfoCard.getElementsByTag("b").get(0).html();
        String name = nameAndParty.split("\\(")[0];
        String altName = nameAndParty.split("\\(")[1].split("\\)")[0];
        Politician politician = new Politician();
        politician.name = name;
        return politician;
    }

    private List<Politician> getPartyMembersMyDocument (Document document) {
        ArrayList<Politician> politicians = new ArrayList<>();
        for (Element member : document.getElementsByClass("toggle-member-info")) {
            Politician politician = new Politician();
            politician.name = member.html();
            politicians.add(politician);
        }
        return politicians;
    }
}
