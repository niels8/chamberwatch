package models;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Setter
public class Party {
    public int partyId;
    public List<Politician> members = new ArrayList<>();
    public Politician partyLeader;
    public String name;
    public String altName;
    public Party() {

    }
    public Party (List<Politician> members, Politician partyLeader, String name) {
        this.members = members;
        this.partyLeader = partyLeader;
        this.name  = name;
    }
    public Party (List<Politician> members, Politician partyLeader, String name, String altName) {
        this.members = members;
        this.partyLeader = partyLeader;
        this.name  = name;
        this.altName = altName;
    }
}
