package models;

import lombok.Setter;

@Setter
public class Comment {
    public int commentId;
    public String body;
    public int score;
    public String userId;
    public Votable votable;
}
