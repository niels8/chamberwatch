package models;

import lombok.Setter;

@Setter
public class VotableVote {
    public int id;
    public Votable votable;
    public Politician politician;
    public boolean isFor;
}
