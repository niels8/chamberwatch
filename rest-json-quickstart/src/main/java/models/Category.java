package models;


import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
public class Category {
    public int categoryId;
    public String name;
    public List<Votable> votables = new ArrayList<>();
    public Category (String name) {
        this.name = name;
    }
    public Category (String name, List<Votable> votables) {
        this.name = name;
        this.votables = votables;
    }
    public void setVotables(List<Votable> votables) {
        this.votables = votables;
    }
}
