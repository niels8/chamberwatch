package models;

import lombok.Setter;

@Setter
public class Politician {
    public int politicianId;
    public String name;
    public Party party;
}
