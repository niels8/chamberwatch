package models;

import enums.VoteStatus;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Setter
public class Votable {
    public int votableId;
    public String name;
    public String type;
    public String date;
    public List<Politician> submitters = new ArrayList<>();
    public String status;
    public List<VotableVote> votes;

    public Votable(String name, String type, String date, List<Politician> submitters, String status, List<VotableVote> votes) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.submitters = submitters;
        this.status = status;
        this.votes = votes;
    }
    public Votable(String name, String type, String date, List<Politician> submitters, List<VotableVote> votes) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.submitters = submitters;
        this.votes = votes;
    }
}
