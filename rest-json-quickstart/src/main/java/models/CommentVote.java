package models;

import lombok.Setter;

@Setter
public class CommentVote {
    public int voteId;
    public String voteType;
    public String userId;
    public Comment comment;
}
