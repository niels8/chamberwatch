package resources;

import logic.businesslogic.CategoryLogic;
import logic.webscraping.CategoriesScraper;
import logic.webscraping.PartiesScraper;
import models.Category;
import models.Votable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/api/category")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryResource {
    @Inject
    CategoryLogic categoryLogic;
    @Inject
    CategoriesScraper scraper;
    @Inject
    PartiesScraper partiesScraper;

    @GET
    @Path("{categoryId}")
    public Response getCategoryById(int categoryId) {
        try {
            return Response.ok(categoryLogic.getCategoryById(categoryId)).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @GET
    public Response getCategories() {
        try {
            return Response.ok(categoryLogic.getCategories()).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }
    @GET
    @Path("scrape")
    public void scrape() throws IOException {
        //scraper.scrapeForDate(16,01,2020);
        partiesScraper.initializeParties();
    }
}
