package resources;

import logic.businesslogic.CategoryLogic;
import logic.businesslogic.VotableLogic;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/votable")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VotableResource {
    @Inject
    VotableLogic votableLogic;
    @GET
    @Path("category/{categoryId}/votable/{votableId}/")
    public Response getVotable(int categoryId, int votableId) {
        try {
            return Response.ok(votableLogic.getVotableById(categoryId, votableId)).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

}

