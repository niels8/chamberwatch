package resources;

import logic.businesslogic.CommentLogic;
import models.Comment;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/api/comments/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommentResource {
    @Inject
    CommentLogic commentLogic;
    @POST
    @Path("{votableId}")
    public Response add(@Context SecurityContext ctx, Comment comment, @PathParam("votableId") int votableId) {
        try {
            commentLogic.addComment(votableId, comment, ctx.getUserPrincipal().getName());
            return Response.status(200).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @DELETE
    @Path("{commentId}")
    public Response delete(@Context SecurityContext ctx, @PathParam("commentId") int commentId) {
        try {
            commentLogic.removeComment(commentId, ctx.getUserPrincipal().getName());
            return Response.status(200).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

}
