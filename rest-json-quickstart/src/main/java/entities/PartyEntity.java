package entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import models.Politician;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
@Getter
@Entity(name = "Party")
public class PartyEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int partyId;
    @OneToMany(mappedBy = "partyMember", cascade = {CascadeType.REMOVE})
    public Set<PoliticianEntity> members;
    @OneToOne
    @JoinColumn(name = "partyleaderId")
    public PoliticianEntity partyLeader;

    public String name;
    public String shortname;
}
