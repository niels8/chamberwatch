package entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;
@Getter
@Entity(name = "Politician")
public class PoliticianEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int politicianId;
    public String name;
    @OneToOne(mappedBy = "partyLeader")
    public PartyEntity party;
    @ManyToOne
    @JoinColumn(name = "partyId")
    public PartyEntity partyMember;

    @OneToMany(mappedBy = "politician", cascade = {CascadeType.REMOVE})
    public Set<VotableVoteEntity> votes;
    @ManyToMany(cascade = {CascadeType.DETACH})
    @JoinTable(
            name = "Submitter",
            joinColumns = {@JoinColumn(name = "politicianId")},
            inverseJoinColumns = {@JoinColumn(name = "votableId")}
    )
    public Set<VotableEntity> votables;
}
