package entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;
@Getter
@Entity(name = "Category")
public class CategoryEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int categoryId;
    public String name;
    @OneToMany(mappedBy = "category")
    public Set<VotableEntity> votables;
}
