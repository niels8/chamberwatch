package entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;
@Getter
@Entity(name = "Comment")
public class CommentEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int commentId;
    public String body;
    public int score;
    public String userId;
    @ManyToOne
    @JoinColumn(name = "votableId", nullable = false)
    public VotableEntity votable;
    @OneToMany(mappedBy = "comment", cascade = {CascadeType.REMOVE})
    public Set<VoteEntity> votes;
}
