package entities;

import enums.VoteStatus;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import models.Category;
import models.Politician;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
@Getter
@Entity(name = "Votable")
public class VotableEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int votableId;
    public String name;
    public String type;
    public String date;
    @ManyToMany(mappedBy = "votables")
    public Set<PoliticianEntity> submitters;
    public String status;
    @OneToMany(mappedBy = "votable", cascade = {CascadeType.REMOVE})
    public Set<VotableVoteEntity> votes;
    @ManyToOne
    @JoinColumn(name="categoryId", nullable=false)
    public CategoryEntity category;
    @OneToMany(mappedBy = "votable", cascade = {CascadeType.REMOVE})
    public Set<CommentEntity> comments;
}
