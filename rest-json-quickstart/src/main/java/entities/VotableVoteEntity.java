package entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import models.Politician;
import models.Votable;

import javax.persistence.*;
@Getter
@Entity(name = "Voter")
public class VotableVoteEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    @ManyToOne
    @JoinColumn(name = "votableId", nullable = false)
    public VotableEntity votable;
    @ManyToOne
    @JoinColumn(name = "politicianId", nullable = false)
    public PoliticianEntity politician;
    public boolean isFor;
}
