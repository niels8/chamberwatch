package entities;


import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;

import javax.persistence.*;
@Getter
@Entity(name = "Vote")
public class VoteEntity extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int voteId;
    public String voteType;
    public String userId;
    @ManyToOne
    @JoinColumn(name = "commentId", nullable = false)
    public CommentEntity comment;
}
