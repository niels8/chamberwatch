package models;

import java.util.ArrayList;
import java.util.List;

public class Party {
    List<Politician> members = new ArrayList<>();
    Politician leader;
    String name;
    String altName;
    public Party (List<Politician> members, Politician leader, String name) {
        this.members = members;
        this.leader = leader;
        this.name  = name;
    }
    public Party (List<Politician> members, Politician leader, String name, String altName) {
        this.members = members;
        this.leader = leader;
        this.name  = name;
        this.altName = altName;
    }
    public String getName() {
        return name;
    }
    public List<Politician> getMembers() {
        return members;
    }

    public String getAltName() {
        if (altName != null) {
            return altName;
        }
        else {
            return name;
        }
    }
}
