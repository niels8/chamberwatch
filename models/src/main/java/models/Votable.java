package models;

import enums.VoteStatus;

import java.util.ArrayList;
import java.util.List;

public class Votable {
    private String name;
    private String type;
    private String date;
    private List<Politician> submitters = new ArrayList<>();
    private VoteStatus status;
    private List<Politician> votersFor = new ArrayList<>();
    private List<Politician> votersAgainst = new ArrayList<>();
    public String getName() {
        return name;
    }
    public List<Politician> getSubmitterName() {
        return submitters;
    }
    public VoteStatus getStatus() {
        return status;
    }

    public Votable(String name, List<Politician> submitters, String type, VoteStatus status) {
        this.name = name;
        this.submitters = submitters;
        this.status = status;
        this.type = type;
    }
    public Votable(String name, List<Politician> submitters, String type, List<Politician> votersFor, List<Politician> votersAgainst) {
        this.name = name;
        this.submitters = submitters;
        this.type = type;
        this.votersFor = votersFor;
        this.votersAgainst = votersAgainst;
        this. status = (votersFor.size() > votersAgainst.size())? VoteStatus.PASSED : VoteStatus.FAILED;
    }
    public Votable(String name, List<Politician> submitters, String type, List<Politician> votersFor, List<Politician> votersAgainst, String date) {
        this.name = name;
        this.submitters = submitters;
        this.type = type;
        this.votersFor = votersFor;
        this.votersAgainst = votersAgainst;
        this. status = (votersFor.size() > votersAgainst.size())? VoteStatus.PASSED : VoteStatus.FAILED;
        this.date = date;
    }
}
