package models;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String name;
    private List<Votable> votables = new ArrayList<>();
    private Law law;
    public String getName() {
        return this.name;
    }
    public List<Votable> getVotables() {
        return this.votables;
    }
    public Law getLaw() {
        return law;
    }
    public boolean hasLaw() {
        return (law != null);
    }
    public Category (String name) {
        this.name = name;
    }
    public void setVotables(List<Votable> votables) {
        this.votables = votables;
    }
}
