package models;

public class Politician {
    private String name;

    public String getName() {
        return name;
    }

    public Politician(String name) {
        this.name = name;
    }
}
