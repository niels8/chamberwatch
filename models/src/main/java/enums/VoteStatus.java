package enums;

public enum VoteStatus {
    AWAITING_VOTE,
    PASSED,
    FAILED,
    CANCELLED;
}
